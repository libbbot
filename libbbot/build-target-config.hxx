// file      : libbbot/build-target-config.hxx -*- C++ -*-
// license   : MIT; see accompanying LICENSE file

#ifndef LIBBBOT_BUILD_CONFIG_HXX
#define LIBBBOT_BUILD_CONFIG_HXX

#include <map>
#include <string>
#include <vector>
#include <iosfwd>

#include <libbutl/path.hxx>
#include <libbutl/optional.hxx>
#include <libbutl/tab-parser.hxx>
#include <libbutl/target-triplet.hxx>

#include <libbbot/export.hxx>
#include <libbbot/version.hxx>

namespace bbot
{
  // Build target configuration matching specific machine names. Used by bbot
  // controllers.
  //
  struct build_target_config
  {
    std::string machine_pattern;             // Machine name pattern.
    std::string name;                        // Configuration name.
    butl::target_triplet target;             // Build target.
    butl::optional<std::string> environment; // Build environment name.
    std::vector<std::string> classes;
    std::vector<std::string> args;           // Note: quoting is preserved.

    // Warning-detecting regular expressions. Note that quoting is preserved.
    //
    std::vector<std::string> warning_regexes;
  };

  struct build_target_configs: std::vector<build_target_config>
  {
    // List of all target configuration class names. Starts with the all and
    // default classes. The rest follows in the same order as in the buildtab.
    //
    std::vector<std::string> classes;

    // A map of derived class names to their bases.
    //
    std::map<std::string, std::string> class_inheritance_map;
  };

  // Parse buildtab stream or file. Throw tab_parsing on parsing error,
  // ios::failure on the underlying OS error.
  //
  // buildtab consists of lines in the following format:
  //
  // <machine-pattern> <target-config> <target>[/<environment>] <classes> [<tgt-config-arg>]* [<warning-regex>]*
  //
  // Note that each <target-config>/<target> pair is expected to be unique in
  // the buildtab.
  //
  using butl::tab_parsing;

  LIBBBOT_EXPORT build_target_configs
  parse_buildtab (std::istream&, const std::string& name);

  LIBBBOT_EXPORT build_target_configs
  parse_buildtab (const butl::path&);
}

#endif // LIBBBOT_BUILD_CONFIG_HXX
