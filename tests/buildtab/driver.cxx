// file      : tests/buildtab/driver.cxx -*- C++ -*-
// license   : MIT; see accompanying LICENSE file

#include <ios>      // ios::failbit, ios::badbit
#include <iostream>

#include <libbutl/utility.hxx> // operator<<(ostream,exception)

#include <libbbot/build-target-config.hxx>

#undef NDEBUG
#include <cassert>

using namespace std;
using namespace butl;
using namespace bbot;

// Usage: argv[0]
//
// Read and parse buildtab from STDIN and serialize the resulted build
// configuration to STDOUT.
//
int
main ()
try
{
  cin.exceptions  (ios::failbit | ios::badbit);
  cout.exceptions (ios::failbit | ios::badbit);

  const build_target_configs& configs (parse_buildtab (cin, "cin"));

  for (const build_target_config& c: configs)
  {
    cout << c.machine_pattern << ' ' << c.name << ' ' << c.target;

    if (c.environment)
      cout << '/' << *c.environment;

    string classes;
    for (const string& cls: c.classes)
    {
      if (!classes.empty ())
        classes += ' ';

      classes += cls;

      auto i (configs.class_inheritance_map.find (cls));
      if (i != configs.class_inheritance_map.end ())
        classes += ':' + i->second;
    }

    if (c.classes.size () == 1)
      cout << ' ' << classes;
    else
      cout << " \"" << classes << '"';

    for (const string& a: c.args)
      cout << ' ' << a;

    for (const string& r: c.warning_regexes)
      cout << " ~" << r;

    cout << '\n';
  }

  return 0;
}
catch (const tab_parsing& e)
{
  cerr << e << endl;
  return 1;
}
