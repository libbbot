# file      : tests/manifest/task-request.testscript
# license   : MIT; see accompanying LICENSE file

test.options += -tq

: valid
:
: Roundtrip the task request manifest.
:
{
  : all-values
  :
  $* <<EOF >>EOF
  : 1
  agent: upsa
  toolchain-name: queue
  toolchain-version: 0.5.0
  interactive-mode: both
  interactive-login: 10.5.0.1:5901
  fingerprint: 1105fb394ee870adb154b7abfbbae5755df7dcef6c81db34e8d1b68d2653734e
  auxiliary-ram: 41943040
  :
  id: a2b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
  name: windows_10-msvc_14
  summary: Windows 10 build 1607 with VC 14 update 3
  EOF

  : no-fingerprint
  :
  $* <<EOF >>EOF
  : 1
  agent: upsa
  toolchain-name: queue
  toolchain-version: 0.5.0
  :
  id: a2b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
  name: windows_10-msvc_14
  summary: Windows 10 build 1607 with VC 14 update 3
  EOF
}

: redefinition
:
{
  : agent
  :
  $* <<EOI 2>'stdin:3:1: error: task request agent redefinition' == 1
  : 1
  agent: upsa
  agent: upsa
  EOI

  : toolchain-name
  :
  $* <<EOI 2>'stdin:3:1: error: task request toolchain name redefinition' == 1
  : 1
  toolchain-name: queue
  toolchain-name: queue
  EOI

  : toolchain-version
  :
  $* <<EOI 2>'stdin:3:1: error: task request toolchain version redefinition' == 1
  : 1
  toolchain-version: 0.5.0
  toolchain-version: 0.5.0
  EOI

  : fingerprint
  :
  $* <<EOI 2>'stdin:3:1: error: task request fingerprint redefinition' == 1
  : 1
  fingerprint: 1105fb394ee870adb154b7abfbbae5755df7dcef6c81db34e8d1b68d2653734e
  fingerprint: 1105fb394ee870adb154b7abfbbae5755df7dcef6c81db34e8d1b68d2653734e
  EOI

  : auxiliary-ram
  :
  $* <<EOI 2>'stdin:3:1: error: auxiliary machines RAM limit redefinition' == 1
  : 1
  auxiliary-ram: 41943040
  auxiliary-ram: 41943040
  EOI
}

: empty
:
{
  : agent
  :
  $* <<EOI 2>'stdin:2:7: error: empty task request agent' == 1
  : 1
  agent:
  EOI

  : toolchain-name
  :
  $* <<EOI 2>'stdin:2:16: error: empty task request toolchain name' == 1
  : 1
  toolchain-name:
  EOI

  : auxiliary-ram
  :
  $* <<EOI 2>'stdin:2:15: error: auxiliary machines RAM limit should be 64-bit unsigned integer' == 1
  : 1
  auxiliary-ram:
  EOI
}

: invalid-toolchain-version
:
$* <<EOI 2>'stdin:2:19: error: invalid task request toolchain version: invalid major version' == 1
: 1
toolchain-version:
EOI

: invalid-fingerprint
:
$* <<EOI 2>'stdin:2:14: error: invalid task request fingerprint' == 1
: 1
fingerprint: 123x
EOI

: missed
:
{
  : agent
  :
  $* <<EOI 2>'stdin:3:1: error: no task request agent specified' == 1
  : 1
  fingerprint: 1105fb394ee870adb154b7abfbbae5755df7dcef6c81db34e8d1b68d2653734e
  EOI
}

: no-machines
:
$* <<EOI 2>'stdin:6:1: error: no task request machines specified' == 1
: 1
agent: upsa
toolchain-name: queue
toolchain-version: 0.5.0
fingerprint: 1105fb394ee870adb154b7abfbbae5755df7dcef6c81db34e8d1b68d2653734e
EOI

: interactive
:
{
  : redefinition
  :
  {
    : mode
    :
    $* <<EOI 2>'stdin:3:1: error: task request interactive mode redefinition' == 1
      : 1
      interactive-mode: true
      interactive-mode: both
      EOI

    : login
    :
    $* <<EOI 2>'stdin:3:1: error: task request interactive login redefinition' == 1
      : 1
      interactive-login: 10.5.0.1:5901
      interactive-login: 10.5.0.1:5901
      EOI
  }

  : invalid
  :
  {
    : mode
    :
    $* <<EOI 2>"stdin:2:19: error: invalid task request interactive mode 'on'" == 1
      : 1
      interactive-mode: on
      EOI

    : login
    :
    $* <<EOI 2>'stdin:2:19: error: empty task request interactive login' == 1
      : 1
      interactive-login:
      EOI
  }

  : no-login
  :
  $* <<EOI 2>'stdin:6:1: error: no task request interactive login specified' == 1
    : 1
    agent: upsa
    toolchain-name: queue
    toolchain-version: 0.5.0
    interactive-mode: both
    EOI

  : unexpected-login
  :
  $* <<EOI 2>'stdin:5:1: error: interactive login specified for non-interactive mode' == 1
    : 1
    agent: upsa
    toolchain-name: queue
    toolchain-version: 0.5.0
    interactive-login: 10.5.0.1:5901
    EOI
}
