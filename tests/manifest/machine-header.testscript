# file      : tests/manifest/machine-header.testscript
# license   : MIT; see accompanying LICENSE file

test.options += -m

: valid
:
: Roundtrip the machine header manifest.
:
{
  $* <<EOF >>EOF
  : 1
  id: a2b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
  name: windows_10-msvc_14
  summary: Windows 10 build 1607 with VC 14 update 3
  role: build
  ram-minimum: 4194304
  EOF
}

: multiple
:
$* <<EOI 2>'stdin:5:1: error: single machine header manifest expected' == 1
: 1
id: a2b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
name: windows_10-msvc_14
summary: Windows 10 build 1607 with VC 14 update 3
:
EOI

: redefinition
:
{
  : id
  :
  $* <<EOI 2>'stdin:3:1: error: machine id redefinition' == 1
  : 1
  id: 123
  id: 123
  EOI

  : name
  :
  $* <<EOI 2>'stdin:3:1: error: machine name redefinition' == 1
  : 1
  name: windows
  name: windows
  EOI

  : summary
  :
  $* <<EOI 2>'stdin:3:1: error: machine summary redefinition' == 1
  : 1
  summary: Windows
  summary: Windows
  EOI

  : ram-maximum
  :
  $* <<EOI 2>'stdin:3:1: error: machine maximum RAM redefinition' == 1
  : 1
  ram-maximum: 4194304
  ram-maximum: 4194304
  EOI

  : role
  :
  $* <<EOI 2>'stdin:3:1: error: machine role redefinition' == 1
  : 1
  role: build
  role: auxiliary
  EOI
}

: empty
:
{
  : id
  :
  $* <<EOI 2>'stdin:2:4: error: empty machine id' == 1
  : 1
  id:
  EOI

  : name
  :
  $* <<EOI 2>'stdin:2:6: error: empty machine name' == 1
  : 1
  name:
  EOI

  : summary
  :
  $* <<EOI 2>'stdin:2:9: error: empty machine summary' == 1
  : 1
  summary:
  EOI
}

: unknown-name
:
$* <<EOI 2>"stdin:2:1: error: unknown name 'x' in machine header manifest" == 1
: 1
x:
EOI

: missed
:
{
  : id
  :
  $* <<EOI 2>'stdin:4:1: error: no machine id specified' == 1
  : 1
  name: windows
  summary: Windows
  EOI

  : name
  :
  $* <<EOI 2>'stdin:4:1: error: no machine name specified' == 1
  : 1
  id: 123
  summary: Windows
  EOI

  : summary
  :
  $* <<EOI 2>'stdin:4:1: error: no machine summary specified' == 1
  : 1
  id: 123
  name: windows
  EOI
}
