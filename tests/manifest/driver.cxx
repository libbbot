// file      : tests/manifest/driver.cxx -*- C++ -*-
// license   : MIT; see accompanying LICENSE file

#include <ios>      // ios_base::failbit, ios_base::badbit
#include <string>
#include <iostream>

#include <libbutl/utility.hxx>             // operator<<(ostream,exception)
#include <libbutl/manifest-parser.hxx>
#include <libbutl/manifest-serializer.hxx>

#include <libbbot/manifest.hxx>

#undef NDEBUG
#include <cassert>

using namespace std;
using namespace butl;
using namespace bbot;

// Usage: argv[0] (-m|-t|-r|-tq|-ts|-rq)
//
// Read and parse manifest from STDIN and serialize it to STDOUT. The
// following options specify the manifest type.
//
// -m  parse machine header manifest
// -t  parse task manifest
// -r  parse result manifest
// -tq parse task request manifest
// -ts parse task response manifest
// -rq parse result request manifest
//
int
main (int argc, char* argv[])
try
{
  assert (argc == 2);
  string opt (argv[1]);

  cin.exceptions  (ios_base::failbit | ios_base::badbit);
  cout.exceptions (ios_base::failbit | ios_base::badbit);

  manifest_parser     p (cin,  "stdin");
  manifest_serializer s (cout, "stdout");

  if (opt == "-m")
    machine_header_manifest (p).serialize (s);
  else if (opt == "-t")
    task_manifest (p).serialize (s);
  else if (opt == "-r")
    result_manifest (p).serialize (s);
  else if (opt == "-tq")
    task_request_manifest (p).serialize (s);
  else if (opt == "-ts")
    task_response_manifest (p).serialize (s);
  else if (opt == "-rq")
    result_request_manifest (p).serialize (s);
  else
    assert (false);

  return 0;
}
catch (const manifest_parsing& e)
{
  cerr << e << endl;
  return 1;
}
catch (const manifest_serialization& e)
{
  cerr << e << endl;
  return 1;
}
